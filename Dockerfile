FROM debian:bullseye

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -qq
RUN apt-get -qy install git puppet puppet-lint ruby

COPY check-code.sh /

ENTRYPOINT /bin/bash
