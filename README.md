Puppet Checker
==============

Runs the following in `${CI_PROJECT_DIR}`:

- `puppet parser validate` for all `.pp` files.
- `puppet-lint` for the whole directory.
- ERB template check for all `.erb` files.
- EPP template check for all `.epp` files.
