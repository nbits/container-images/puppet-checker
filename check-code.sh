#!/bin/bash

set -ex

if [ -z "${CI_PROJECT_DIR}" ]; then
	echo "Error: CI_PROJECT_DIR not set."
	exit 1
elif [ ! -d "${CI_PROJECT_DIR}" ]; then
	echo "Error:CI_PROJECT_DIR=${CI_PROJECT_DIR} is not a directory."
	exit 1
fi

while read f; do
	puppet parser validate ${f}
	puppet-lint ${f}
done < <( git -C ${CI_PROJECT_DIR} ls-tree -r HEAD --name-only | grep '\.pp$' )

git -C ${CI_PROJECT_DIR} ls-tree -r HEAD --name-only | grep '\.erb$' \
	| while read template ; do \
		echo "Checking ERB template $template"
		erb -x -T '-' "$template" | ruby -c
	done

git -C ${CI_PROJECT_DIR} ls-tree -r HEAD --name-only | grep '\.epp$' \
	| while read template ; do \
		echo "Checking EPP template $template"
		puppet epp validate "$template"
	done
